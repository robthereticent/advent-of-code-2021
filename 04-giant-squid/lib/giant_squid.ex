defmodule GiantSquid do
  @moduledoc """
  Documentation for `GiantSquid`.
  """

  @doc """
  processes day 1
  """
  def process(line_stream) do
    line_stream
    |> parse()
    |> play()
  end

  defp parse(line_stream) do
    [sequence_line | boards] = Enum.to_list(line_stream)
    sequence = sequence_line
               |> String.trim()
               |> String.split(",")
               |> Enum.map(&String.to_integer/1)

    boards = extract_board(boards, [])
    {sequence, boards}
  end

  defp extract_board([], acc) do
    Enum.reverse(acc)
  end

  defp extract_board(["\n" | rest], acc) do
    {board_lines, rest} = Enum.split(rest, 5)
    board = board_lines
            |> Enum.map(&String.trim/1)
            |> Enum.map(&String.split(&1, " ", trim: true))
            |> Enum.map(fn (l) ->
              Enum.map(l, &{String.to_integer(&1), false})
            end)
    
    extract_board(rest, [board | acc])
  end

  defp play(state, top_score \\ 0)

  defp play({[], boards}, top_score) do
    raise "out of numbers"
  end

  defp play({_sequence, []}, 0) do
    raise "out of boards but no score"
  end

  defp play({_sequence, []}, top_score) do
    top_score
  end

  defp play({[number | remainder], boards}, 0) do
    marked_boards = mark(boards, number)

    top_score = case marked_boards do
      [board] -> top_pre_score([board]) * number
      _ -> 0
    end

    unfinished_boards = Enum.filter(
      marked_boards,
      predicate_not(
        predicate_or(
          &row_marked?/1,
          &column_marked?/1
        )
      )
    )

    play({remainder, unfinished_boards}, top_score)
  end

  defp play({_sequence, _boards}, top_score) do
    top_score
  end

  defp mark(boards, number) do
    boards
    |> get_and_update_in(
      [
        Access.all(), # all boards
        Access.all(), # all rows
        fn
          (:get_and_update, data, next) ->
            {_number, value} = Enum.find(
              data,
              {number, nil},
              fn
                {^number, marked} -> true
                (_) -> false
              end
            )

            case next.(value) do
              {get, update} -> {get, Enum.map(
                  data,
                  fn
                    {^number, _marked} -> {number, update}
                    {n, marked} -> {n, marked}
                  end
              )}
              :pop -> {value, Enum.filter(
                  data,
                  fn
                    {^number, _marked} -> false
                    {n, marked} -> true
                  end
              )}
            end
        end,
      ],
      fn
        nil -> :pop
        false -> {false, true}
        true -> {true, true}
      end
    )
    |> Kernel.elem(1)
  end

  defp top_pre_score(boards) do
    boards
    |> Enum.filter(predicate_or(&row_marked?/1, &column_marked?/1))
    |> Enum.map(&score_board/1)
    |> Enum.max(&>=/2, fn () -> 0 end)
  end

  defp row_marked?(board) do
    Enum.any?(
      board,
      fn (row) ->
        Enum.all?(
          row,
          fn {n, marked} -> marked end
        )
      end
    )
  end

  def column_marked?(board) do
    Enum.map(
      0..((board |> hd() |> length()) - 1),
      fn (index) ->
        Enum.all?(
          board,
          fn (row) -> row |> Enum.at(index) |> Kernel.elem(1) end
        )
      end
    )
    |> Enum.any?()
  end

  defp predicate_or(f1, f2) do
    fn (x) -> f1.(x) or f2.(x) end
  end

  defp predicate_not(fun) do
    fn (x) -> x |> fun.() |> Kernel.not() end
  end

  defp score_board(board) do
    board
    |> Enum.flat_map(
      fn (row) ->
        row
        |> Enum.filter(
          fn {n, marked} -> !marked end
        )
        |> Enum.map(fn {n, marked} -> n end)
      end
    )
    |> Enum.sum()
  end
end
