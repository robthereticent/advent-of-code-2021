defmodule GiantSquidTest do
  use ExUnit.Case
  doctest GiantSquid

  test "computes the answer" do
    result = "priv/test-input"
    |> File.stream!()
    |> GiantSquid.process()

    assert result == 1924
  end

  test "checks whether a column is marked" do
    board = [
      [{ 1, true}, { 2, false}, { 3, false}, { 4, false}, { 5, false}],
      [{ 6, true}, { 7, false}, { 8, false}, { 9, false}, {10, false}],
      [{11, true}, {12, false}, {13, false}, {14, false}, {15, false}],
      [{16, true}, {17, false}, {18, false}, {19, false}, {20, false}],
      [{21, true}, {22, false}, {23, false}, {24, false}, {25, false}],
    ]

    assert GiantSquid.column_marked?(board) == true
  end
end
