alias GiantSquid

System.argv()
|> Enum.at(0)
|> File.stream!()
|> GiantSquid.process()
|> IO.puts()
