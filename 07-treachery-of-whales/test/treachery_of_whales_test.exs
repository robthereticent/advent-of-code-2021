defmodule TreacheryOfWhalesTest do
  use ExUnit.Case
  doctest TreacheryOfWhales

  test "processes the data" do
    result = "priv/test-input"
             |> File.stream!()
             |> TreacheryOfWhales.process()

    assert result == 168
  end
end
