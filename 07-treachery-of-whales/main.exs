alias TreacheryOfWhales

System.argv()
|> hd()
|> File.stream!()
|> TreacheryOfWhales.process()
|> IO.puts()
