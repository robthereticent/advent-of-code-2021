defmodule TreacheryOfWhales do
  @moduledoc """
  Documentation for `TreacheryOfWhales`.
  """

  # cost function is now d(d+1) / 2 where d is the distance
  # lagrangian is d + 1/2 which is the same as least-squares but shifted by 1/2
  # that's why we subtract 1/2 after doing the average (least-squares solution)
  def process(line_stream) do
    positions = parse(line_stream)
    best = positions
           |> Enum.reduce({0, 0}, fn(x, {sum, count}) -> {sum + x, count + 1} end)
           |> (fn {sum, count} -> sum / count - 1/2 end).()
           |> round()

    positions
    |> Enum.map(&cost(&1, best))
    |> Enum.sum()
  end

  defp cost(p1, p2) do
    dist = abs(p1 - p2)
    Integer.floor_div(dist * (dist + 1), 2)
  end

  def process1(line_stream) do
    positions = line_stream
                |> parse()
                |> Enum.sort()

    l = length(positions)
    median = case Integer.mod(l, 2) do
      0 -> positions
        |> Enum.slice(Integer.floor_div(l, 2) - 1, 2)
        |> Enum.sum()
        |> Integer.floor_div(2)
      1 -> Enum.at(positions, Integer.floor_div(l - 1, 2))
    end

    positions
    |> Enum.map(&abs(&1 - median))
    |> Enum.sum()
  end

  defp parse(line_stream) do
    line_stream
    |> Stream.take(1)
    |> Enum.to_list()
    |> hd()
    |> String.trim()
    |> String.split(",")
    |> Enum.map(&String.to_integer/1)
  end
end
