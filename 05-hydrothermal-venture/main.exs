alias HydrothermalVenture

System.argv()
|> File.stream!()
|> HydrothermalVenture.process()
|> IO.puts()
