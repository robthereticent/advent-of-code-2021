defmodule HydrothermalVentureTest do
  use ExUnit.Case
  doctest HydrothermalVenture

  test "does the processing" do
    result = "priv/test-input"
             |> File.stream!()
             |> HydrothermalVenture.process()
    assert result == 12
  end
end
