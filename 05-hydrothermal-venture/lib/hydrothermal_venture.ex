defmodule HydrothermalVenture do
  @moduledoc """
  Documentation for `HydrothermalVenture`.
  """

  def process(line_stream) do
    line_stream
    |> parse()
    |> Stream.flat_map(&intervening_points/1)
    |> tally_points()
    |> Enum.filter(fn {pt, count} -> count >= 2 end)
    |> length()
  end

  defp parse(lines) do
    lines
    |> Stream.map(&String.trim/1)
    |> Stream.map(&String.split(&1, " -> "))
    |> Stream.map(fn (points) ->
      points
      |> Enum.map(&String.split(&1, ","))
      |> Enum.map(fn (coord) -> Enum.map(coord, &String.to_integer(&1)) end)
    end)
  end

  defp intervening_points([[x, y1], [x, y2]]) do
    rectified_range(y1, y2)
    |> Enum.map(&{x, &1})
  end

  defp intervening_points([[x1, y], [x2, y]]) do
    rectified_range(x1, x2)
    |> Enum.map(&{&1, y})
  end

  defp intervening_points([[x1, y1], [x2, y2]]) do
    Enum.zip(rectified_range(x1, x2), rectified_range(y1, y2))
  end

  defp rectified_range(a1, a2) when a1 > a2 do
    a1..a2//-1
  end

  defp rectified_range(a1, a2) do
    a1..a2
  end

  defp tally_points(point_stream) do
    Enum.reduce(
      point_stream,
      %{},
      fn (pt, counts) ->
        update_in(
          counts,
          [Access.key(pt, 0)],
          &(&1 + 1)
        )
      end
    )
  end
end
