alias Dive

System.argv()
|> hd()
|> File.stream!()
|> Dive.process()
|> IO.puts()
