defmodule DiveTest do
  use ExUnit.Case
  doctest Dive

  test "greets the world" do
    input = File.stream!("priv/test-input")
    assert Dive.process(input) == 900
  end
end
