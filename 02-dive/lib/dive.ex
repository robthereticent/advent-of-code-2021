defmodule Dive do
  @moduledoc """
  Documentation for `Dive`.
  """

  def main(_argv) do
    File.stream!("priv/input")
    |> process()
    |> IO.puts()
  end

  def process(input_stream) do
    input_stream
    |> Stream.map(&parse/1)
    |> Stream.map(&scale(Kernel.elem(&1, 0), Kernel.elem(&1, 1)))
    |> Stream.scan({0, 0}, &apply_aim/2)
    |> Enum.reduce({0,0}, &travel/2)
    |> Tuple.product()
  end

  defp parse(line) do
    [dir_s, dist_s] = String.split(line)
    direction = case dir_s do
      "forward" -> {1, 0}
      "down" -> {0, 1}
      "up" -> {0, -1}
    end
    distance = dist_s
               |> Integer.parse()
               |> Kernel.elem(0)

    {direction, distance}
  end

  defp scale({x, d}, scale) do
    {x * scale, d * scale}
  end

  defp apply_aim({x_new, a_delta}, {_x_prev, a_prev}) do
    {x_new, a_prev + a_delta}
  end

  defp travel({x_delta, aim}, {x_cur, d_cur}) do
    {x_cur + x_delta, d_cur + x_delta * aim}
  end
end
