defmodule SonarSweep do
  @moduledoc """
  Documentation for `SonarSweep`.
  """

  def main([filename | _]) do
    File.stream!(filename)
    |> process()
    |> IO.puts()
  end

  @doc """
  Process the given stream of lines
  """
  def process(line_stream) do
    line_stream
    |> Stream.map(&String.trim/1)
    |> Stream.map(&Integer.parse/1)
    |> Stream.map(&Kernel.elem(&1, 0))
    |> Stream.chunk_every(3, 1, :discard)
    |> Stream.map(&Enum.sum/1)
    |> Enum.reduce({nil, 0}, &tally_increases/2)
    |> Kernel.elem(1)
  end

  defp tally_increases(cur, {nil, total}) do
    {cur, total}
  end

  defp tally_increases(cur, {prev, total}) do
    case cur > prev do
      true -> {cur, total + 1}
      _ -> {cur, total}
    end
  end
end
