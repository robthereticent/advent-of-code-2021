defmodule SonarSweepTest do
  use ExUnit.Case
  doctest SonarSweep

  test "does the thing" do
    filename = "priv/test-input"
    
    input = File.stream!(filename)
    assert SonarSweep.process(input) == 5
  end
end
