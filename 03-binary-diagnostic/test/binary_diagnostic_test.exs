defmodule BinaryDiagnosticTest do
  use ExUnit.Case
  doctest BinaryDiagnostic

  test "computes the thing" do
    result = "priv/test-input"
             |> File.stream!()
             |> BinaryDiagnostic.process()
    assert result == 230
  end
end
