alias BinaryDiagnostic

System.argv()
|> hd()
|> File.stream!()
|> BinaryDiagnostic.process()
|> IO.puts()
