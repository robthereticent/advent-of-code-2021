defmodule BinaryDiagnostic do
  @moduledoc """
  Documentation for `BinaryDiagnostic`.
  """

  def process(line_stream) do
    bit_lists = Enum.map(line_stream, &parse/1)

    oxy_gen_rating = bit_lists
                     |> consensus_filter(&most_common/1)
                     |> bits_to_string()
                     |> String.to_integer(2)

    co2_scrubber_rating = bit_lists
                     |> consensus_filter(&least_common/1)
                     |> bits_to_string()
                     |> String.to_integer(2)

    oxy_gen_rating * co2_scrubber_rating
  end

  defp parse(bitstring) do
    bitstring
    |> String.trim()
    |> String.to_charlist()
    |> Enum.map(&(&1 - 0x30)) # excessively clever
  end

  defp consensus_filter(lists, discriminant_generator) do
    consensus_filter(lists, discriminant_generator, 0)
  end

  defp consensus_filter([], _discriminant_generator, _position) do
    raise "nothing left"
  end

  defp consensus_filter([list], _discriminant_generator, _position) do
    list
  end

  defp consensus_filter(lists, discriminant_generator, position) do
    {count, bit_counts} = Enum.reduce(lists, {1, []}, &tally_bits/2)

    if position > length(bit_counts) do
      raise "we've gone past the end"
    end

    bit_to_match = discriminant_generator.(count).(Enum.at(bit_counts, position))

    filtered = Enum.filter(lists, &(Enum.at(&1, position) == bit_to_match))

    consensus_filter(filtered, discriminant_generator, position + 1)
  end

  defp tally_bits(new_bits, {count, []}) do
    {count, new_bits}
  end

  defp tally_bits(new_bits, {count, bit_counts}) do
    new_counts = bit_counts
                 |> Enum.zip(new_bits)
                 |> Enum.map(&Tuple.sum/1)
    
    {count + 1, new_counts}
  end

  @doc """
  returns a function that takes a count of 1s in a column and returns
  the most common bit given the `total` number of rows
  """
  defp most_common(total) do
    threshold = majority_threshold(total)
    fn (col_count) -> bool_to_bit(col_count >= threshold) end
  end

  @doc """
  returns a function that takes a count of 1s in a column and returns
  the least common bit given the `total` number of rows
  """
  defp least_common(total) do
    threshold = majority_threshold(total)
    fn (col_count) -> bool_to_bit(col_count < threshold) end
  end

  defp majority_threshold(row_count) do
    threshold = case Integer.mod(row_count, 2) do
      0 -> row_count / 2
      1 -> (row_count + 1) / 2
    end
  end

  defp get_most_and_least_common({count, bit_counts}) do

    most_common = Enum.map(bit_counts, most_common(count))
    least_common = Enum.map(bit_counts, least_common(count))
    [most_common, least_common]
  end

  defp bool_list_to_int(bool_list) do
    bool_list
    |> bools_to_bits()
    |> bits_to_string()
    |> String.to_integer(2)
  end

  defp bools_to_bits(bool_list) do
    bool_list
    |> Enum.map(&bool_to_bitstr/1)
  end

  defp bits_to_string(bit_list) do
    bit_list
    |> Enum.map(&Kernel.to_string/1)
    |> Enum.reduce(&Kernel.<>/2)
    |> String.reverse()
  end

  defp bool_to_bit(true), do: 1
  defp bool_to_bit(false), do: 0

  defp bool_to_bitstr(true), do: "1"
  defp bool_to_bitstr(false), do: "0"
end
